// COMMENTS	
// Comments are parts of code that are ignored by the language.
// there are 2 types of comments in Javascript: 
// Single line comments are denoted by 2 slashes - //
// Multi-line comments are denoted by a slash and asterisk pair - /**/

/* 
	This is how you write a multi-line comment 
*/

// USING THE CONSOLE OBJECT
// console.log("Hello World")
// console.log displays a message in the developer tools console, with whatever is inside of the parenthesis as the message.
// Its good practice to always have a console.log message to check when first linking your JS file to your HTML file to check if the link is correct.

/*	
	SYNTAX AND STATEMENTS 
	Syntax: 
	In programming, syntax is the set of rules that describes how statements must be constructed.

	Statement: 
	In a programming language, statements are instructions that we tell the computer to perform.	

	JS statements usually end with a semicolon (;)

	Semicolons are NOT required in JavaScript, but we will use them to help train us to locate where a statement ends.
*/


/*
	VARIABLES 
	Variables are a way for us to store data.
	Variables are a named location for a stored value.

	Guide to writing variables: 
	1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.

	2. Variable names should start with a lowercase character, use camelCase for multiple words. 

	thisIsCamelCase

	3. For constant variables, use the 'const' keyword instead of 'let'

	4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion
	  		- Always try to make your code human-readable
*/

// let name = "Obi"; 

// console.log(name);

// console.log("Obi");

// let productName = 'desktop computer'; 
// let productPrice = 18999;
// const name = "Obi";

// console.log(name); 

// keyword VARIABLE assignment operator (=) VALUE

/*
	When we DECLARE a variable (create it for the first time), we must use a keyword such as let or const

	After the keyword, is the variable name (see abpve for variable name rules)

	Finally, the value is any valid JavaScript datatype. 

	If we REASSIGN (change) an existing variable, we do NOT include the keyword.

	cons variables cannot be reassigned.
*/

// let student = "Paul" 
// const student = "Paul"

// student = Hiro;

// console.log(student)

// DATA TYPES 
// Strings 
// Any alphanumeric (letters/numbers plus symbols) combination enclosed in quotation marks (can be empty)

// let country = "Philippines";
// let province = "Metro Manila"; 
// let fullAddress = "Metro Manila, Philippines 1711";
// let emptyString = "";
// let phoneNumber = "090123456789";
// let singleQuotes = 'This is also a string!';

// single quotes vs. double quotes

// "Jino's Pencil" 

// 'He said "hello"'

// "He said 'hello'" 

// console.log("1" + "1")

// When a string is added to another string, Javascript performs something called cancatenation.

// Numbers 
// let grade = 98.7
// let headCount = 26 
// let notPhoneNumber = 9123456789

// // When numbers are used, JavaScript can perform normal arithmetic operations

// // Boolean: 
// let isMarried = false; 
// let inGoodConduct = true; 

// Null: 
// Null is the intentional absence of a value.

// let spouse = null; 

// // Undefined:
// // Represents the state of a variable that has been declared but has no assigned value

// let fullName;

// console.log(fullName);

// fullName = "Obi Chukwu"

// DATA STRUCTURES
// Arrays 
// Arrays are used in grouping data that are related. Grouping different data together.
// Arrays are data structures but not data types
	let shoppingList = ["Milk", "Eggs", "Bread"]
	let numbers = [1, 2, 3, 4] 
	let booleans = [true, false]
	let nullAndUndefined = [null, undefined] 

// Objects 
// objects are data structures and data types
	let person = {
	fullName: "Obi Chukwu", 
	isMarried: false, 
	age: 32,
	occupation: "Instructor"
	}
// Objects have PROPERTIES

// JavaScript Operators
// Assignment Operators 
/*
	Basic assignment operator (=)
	The assignment operator adds the value of the right operand to a variable
*/

	let assignmentNumber = 8; 

/*
	Addition assignment operator (+=)
	The addition assignment operator adds the value of the right operand to a variable and assigns the result to that variable
*/

	// console.log(assignmentNumber + 2)
	// console.log(assignmentNumber += assignmentNumber) 
	// console.log(assignmentNumber)

/*
	Substraction/Multiplication/Division Assignment Operator (-=, *=, /=) 
*/

// 	assignmentNumber -= 2

// 	assignmentNumber *= 2 

// 	assignmentNumber /= 2 

// 	console.log(assignmentNumber)	

// 	Arithmetic Operators 
	let x = 10
	let y = 5 

	let sum = x + y 

	let difference = x - y 

	let quotient = x / y 

	let remainder = x % y
//% = modulo operator: the purpose of the modulo operator is to find a remainder. The modulo operator checkS if a no is even or odd.

	let increment1 = ++x
	let increment2 = x++
// 	Increment is just to add 1

	let decrement1 = --x
	let decrement2 = x--
// 	decrement is just to substract 1

//	console.log(++x)
// 	console.log(x++)
// 	console.log(x)

// 	TYPE COERCION
// 	let stringNum = "1" + "1" 
// 	let newString = "cat" + "dog"
// 	let newString = "I have" + "five" + "cats" + "and four dogs"

	let newNum = 10.444 
	console.log(newNum.toFixed(3) + newNum.toFixed(3))
// ToFixed does two things. The first thing is to change the decimal places of the value. the second part is it turns the number or value to a string.
	
	// console.log(1000 + parseInt("1000"))

	let coercion = 5 + parseInt("5")
	// use parseInt() for whole number, or parseFloat() for numbers with decimals

	// console.log(coercion.toFixed(2))
	// toFixed(num) can be used to change how many decimal places are shown depending on what num is. Keep in mind the resulting value will be a STRING

	// console.log(typeof 1)
	// typeof can be used 
	let newString = "I have" + 1 + 1 + "cats"
// 	When 2 strings are put or added together, it is called concatenation

// 	console.log(stringNum)

// 	Equality Operator 

// let juan = 'juan'; // Not an Equality Operator. (=) is an Assignment operator. 

// 	Loose equality operator
// 	Loose equality checks if the VALUE is the same, regardless of type
// 	console.log(1 == 1)
// 	console.log(1 == 2)
// 	console.log(1 == '1')
// 	console.log('juan' == juan) 
// 	console.log(0 == false)
// 	0 is what is called a "falsy" value. Falsy values are those values that if you convert to boolean, they are all the same (false) 
// 	Other falsy values: 
// 	Undefined 
// 	null 
// 	NaN (Not a Number)
// 	""
// We don't really use loose equality operator.

// Strict equality operator
// // Strict equality checks if the VALUE and the TYPE is the same
// console.log(1 === "1")
// console.log(0 === false)

// Loose inequality operator
// Loose inequality checks if the VALUES are NOT the same // 
// console.log(5 != 5)
// console.log(5 !="5") 

// Strict inequality 
// Strict inequality checks if the VALUE is NOT the same and the TYPE is also NOT the same
// console.log(5 !== 5)
// console.log(5 !== "5")

// RELATIONAL OPERATORS 
// Greater than

// let x = 10 
// let y = 5 

// console.log(x > y) 

// Greater than or equal to 

// console.log(10 >= 10) 

// less than 

// console.log(y < x) 

// less than or equal to 

// console.log(15 <= 7) 
// console.log(7 <= 8)

// LOGICAL OPERATORS
let isLegalAge = true; 
let isRegistered = false; 

let areAllRequirementsMet = isLegalAge && isRegistered; //&& = AND operator
// All conditions (Both) have to be true for it to be true

// console.log(areAllRequirementsMet)

let otherExample = !isLegalAge && isRegistered

let areSomeRequirementsMet = isLegalAge || isRegistered; // || OR operator
// One of the conditions must be true for it to be true.

// console.log(areSomeRequirementsMet)

let areSomeRequirementsMetNotMet = !(isLegalAge && isRegistered); // NOT operator (!) reverses the logic
 

// console.log("I have " + num + " cats")

// TEMPLATE LITERALS 

// let num = 16 

// console.log(`I have ${num} cats`) //`` = backticks
   // console.log(`I have ${3 + 1} cats`)
   // Result: I have 4 cats

   // If you write "I have " + 3 + 1 + " cats"
   // Result: I have 31 cats

// MINI ACTIVITY 
// Step 1: Create a variable named song with a value of your favorite song as a string. 
// Step 2: Create another variable named artist with a value of the artist who performed your favorite song as a string. 
// Step 3: Log the following message in the console using template literals: 
// "My favorite song is SONG by ARTIST"

// let song = "essence"
// let artist = "wizkid"

// console.log(`My favorite song is ${song} by ${artist}`)

// FUNCTIONS

// let song = "essence"
// let artist = "wizkid"

// function declaration
function faveSong(song, artist){
	console.log(`My favorite song is ${song} by ${artist}`)	
}

faveSong("Ye", "Burna Boy")

// console.log("Hello World")

// function sayHello(){
// 	console.log("Hello World!");
// };

// sayHello() //Invoke the function
// creating a function is not the same as invoking a function. Creating a function is building it up, setting it up. Invoking is calling it up

// function addNum(num){
// 	console.log(num)
// };

// change this function so that it can add the number that is passed to it to itself and log the result in the console.

function addNum(num){
	console.log(num + num)
};

